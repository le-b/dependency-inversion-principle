package at.hakwt.swp4.dip;

public interface BusinessProcessStep {

    void process();

}
