package at.hakwt.swp4.dip.custominjector;

import at.hakwt.swp4.dip.*;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class DIPCustomInjectorMain {

    public static void main(String[] args) throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        DependencyInjector dependencyInjector = new DependencyInjector();

        BusinessProcess customerImportProcess = new ConfigureableBusinessProcess("CustomerImporter");
        List<BusinessProcessStep> steps = new ArrayList<>();

        steps.add(dependencyInjector.createInstanceOf(LoadFromCsvStep.class));
        steps.add(dependencyInjector.createInstanceOf(DataCanBeStoredStep.class));
        steps.add(dependencyInjector.createInstanceOf(StoreDataStep.class));

        customerImportProcess.configure(steps);
        customerImportProcess.run();

        BusinessProcess christmasPresentsProcess = new ConfigureableBusinessProcess("ChristmasPresents");
        List<BusinessProcessStep> christmasSteps = new ArrayList<>();

        christmasSteps.add(dependencyInjector.createInstanceOf(ChoosePresents.class));
        christmasSteps.add(dependencyInjector.createInstanceOf(BuyPresents.class));
        christmasSteps.add(dependencyInjector.createInstanceOf(SendPresentStep.class));

        christmasPresentsProcess.configure(christmasSteps);
        christmasPresentsProcess.run();




    }


}
