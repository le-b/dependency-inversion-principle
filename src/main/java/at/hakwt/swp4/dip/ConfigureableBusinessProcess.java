package at.hakwt.swp4.dip;

import java.util.ArrayList;
import java.util.List;

/**
 * A configureable implementation of a {@link BusinessProcess}. The
 * {@link #configure(List)} method must be initially called to set
 * the steps for this process.
 */
public class ConfigureableBusinessProcess implements BusinessProcess
{

    private String name;

    public ConfigureableBusinessProcess(String name) {
        this.name = name;
        this.steps = new ArrayList<>();
        System.out.println("--------------- " + name + " ------------");
    }

    private List<BusinessProcessStep> steps;

    public ConfigureableBusinessProcess() {
        this.steps = new ArrayList<>();
    }

    public ConfigureableBusinessProcess(List<BusinessProcessStep> steps) {
        this.steps = new ArrayList<>();
        configure(steps);
    }

    public void configure(List<BusinessProcessStep> services) {
        this.steps.addAll(services);
    }

    public void run() {
        for(BusinessProcessStep step : steps) {
            step.process();
        }
    }


}
