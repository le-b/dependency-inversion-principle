package at.hakwt.swp4.dip.springframework;

public class TwitterSender implements SocialMediaSender
{
    @Override
    public void sendMassage(String message) {
        System.out.println("Sending message " + message + " to Twitter!");
    }
}
