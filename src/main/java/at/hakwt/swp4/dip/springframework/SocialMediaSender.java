package at.hakwt.swp4.dip.springframework;

public interface SocialMediaSender
{
    public void sendMassage(String message);
}
